'use strict';

/**
 * menu-hero-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::menu-hero-section.menu-hero-section');
