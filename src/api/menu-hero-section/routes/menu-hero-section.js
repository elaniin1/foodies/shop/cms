'use strict';

/**
 * menu-hero-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::menu-hero-section.menu-hero-section');
