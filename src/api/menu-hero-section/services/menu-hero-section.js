'use strict';

/**
 * menu-hero-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::menu-hero-section.menu-hero-section');
