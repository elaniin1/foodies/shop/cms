'use strict';

/**
 * landing-finder-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::landing-finder-section.landing-finder-section');
