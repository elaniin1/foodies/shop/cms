'use strict';

/**
 * landing-finder-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::landing-finder-section.landing-finder-section');
