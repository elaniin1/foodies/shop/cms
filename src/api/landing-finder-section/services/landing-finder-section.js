'use strict';

/**
 * landing-finder-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::landing-finder-section.landing-finder-section');
