'use strict';

/**
 * landing-about-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::landing-about-section.landing-about-section');
