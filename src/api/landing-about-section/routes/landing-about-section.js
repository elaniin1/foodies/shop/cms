'use strict';

/**
 * landing-about-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::landing-about-section.landing-about-section');
