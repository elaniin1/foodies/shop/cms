'use strict';

/**
 * landing-about-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::landing-about-section.landing-about-section');
