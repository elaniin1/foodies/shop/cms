'use strict';

/**
 * landing-benefits-app-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::landing-benefits-app-section.landing-benefits-app-section');
