'use strict';

/**
 * landing-benefits-app-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::landing-benefits-app-section.landing-benefits-app-section');
