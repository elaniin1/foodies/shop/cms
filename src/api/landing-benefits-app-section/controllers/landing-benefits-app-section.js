'use strict';

/**
 * landing-benefits-app-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::landing-benefits-app-section.landing-benefits-app-section');
