'use strict';

/**
 * landing-hero-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::landing-hero-section.landing-hero-section');
