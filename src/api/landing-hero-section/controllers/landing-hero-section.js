'use strict';

/**
 * landing-hero-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::landing-hero-section.landing-hero-section');
