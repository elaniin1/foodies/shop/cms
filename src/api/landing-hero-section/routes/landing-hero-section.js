'use strict';

/**
 * landing-hero-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::landing-hero-section.landing-hero-section');
