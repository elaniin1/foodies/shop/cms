'use strict';

/**
 * app-benefit controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::app-benefit.app-benefit');
