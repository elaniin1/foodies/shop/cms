'use strict';

/**
 * app-benefit router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::app-benefit.app-benefit');
