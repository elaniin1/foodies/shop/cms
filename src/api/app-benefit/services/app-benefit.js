'use strict';

/**
 * app-benefit service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::app-benefit.app-benefit');
