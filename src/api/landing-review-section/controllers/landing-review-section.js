'use strict';

/**
 * landing-review-section controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::landing-review-section.landing-review-section');
