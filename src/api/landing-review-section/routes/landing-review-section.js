'use strict';

/**
 * landing-review-section router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::landing-review-section.landing-review-section');
