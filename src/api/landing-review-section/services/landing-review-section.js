'use strict';

/**
 * landing-review-section service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::landing-review-section.landing-review-section');
