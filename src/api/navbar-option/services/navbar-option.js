'use strict';

/**
 * navbar-option service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::navbar-option.navbar-option');
