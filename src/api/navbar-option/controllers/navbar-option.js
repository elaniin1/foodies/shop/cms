'use strict';

/**
 * navbar-option controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::navbar-option.navbar-option');
