import { useCallback, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

const SLUG_WHOLE_DB = 'custom:db';

// this is a workaround to make global import work
// change this value to make global import work for your project
// if the collection is deleted or renamed, this value will be invalid so you need to change it to make global import work again
const SLUG_SOME_SLUG_TO_MAKE_GLOBAL_IMPORT_WORK = 'api::landing-hero-section.landing-hero-section';

export const useSlug = (direction) => {
  const { pathname } = useLocation();

  const [slug, setSlug] = useState('');

  console.log('useSlug direction:', direction);
  console.log('useSlug slug:', slug);

  useEffect(() => {
    const [kind, slug] = pathname.split('/').slice(-2);

    if (['collectionType', 'singleType'].indexOf(kind) > -1) {
      setSlug(slug);
      return;
    }

    if (direction === 'import') {
      setSlug(SLUG_SOME_SLUG_TO_MAKE_GLOBAL_IMPORT_WORK);
      return;
    }

    setSlug(SLUG_WHOLE_DB);
  }, [pathname, setSlug]);

  const isSlugWholeDb = useCallback(() => {
    return slug === SLUG_WHOLE_DB;
  }, [slug]);

  return {
    slug,
    isSlugWholeDb,
  };
};
