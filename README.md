# :hamburger: Welcome to Foodies CMS

For this project we are using [Strapi](https://strapi.io/), a headless CMS that allows you to create and manage your content.

### Running the project in your local machine

#### 1. Cloning the project

```
git clone https://gitlab.com/elaniin1/foodies/shop/cms.git
```

#### 2. Installing dependencies

```
cd cms
```

```
yarn
```

#### 3. Running the project

For this step you need to have a local instance of PostgreSQL running and pgAdmin. If you don't have it installed yet, you can download it from [here](https://www.postgresql.org/download/).

Note: Please remember your local PostgreSQL credentials, you will need them in the next step.

##### 3.1 Craeting a database

In your pgAdmin instance, create a new database called `foodies_cms_db_local` by expanding the `Servers` then expanding `PostgreSQL ##` and right clicking on `Databases` and selecting `Create > Database...`. Then fill the `Database` field with `foodies_cms_db_local` and click on `Save`.

##### 3.2 Creating a .env file

Follow the example of the `.env.example` file and create a `.env` file in the root of the project. You will need to fill the `DATABASE_PASSWORD` fields with your local PostgreSQL credentials.

You also need to fill the `Strapi` related fields with the credentials provided by the dev.

##### 3.3 Run the project

Now you should be ready to go. Run the following command to start the project:

```
yarn develop
```

#### 4. Creating an admin user

Once the project is running, you can access the admin panel by going to `http://localhost:1337/admin`. You will be prompted to create an admin user. Please do so.

#### 5. Dumping the database

Once you have created the admin user, you can dump the database by going to the impor and export section of the admin panel.

Click on the `Import` button and select the `.json` file provided by the dev. This will populate the database with the initial data.

##### - Exporting the database

If you need to export the database, you can do so by clicking on the `Export` button in the import and export section of the admin panel.

Just set the deepness to 6 to improve the performance of the export, for this project we don't need more than that, that's the maximun of our schemas.

#### 6. Giving permissions to the public role

Now you need to give permissions to the public role in order to be able to fetch the data from your Foodies app.

Go to the `Settings` section of the admin panel in the `users & persmissions plugin` and click on the `Roles` tab.

Click on the `Public` role and give read (find and finde one) permissions to the following collections:

- App-benefit
- Footer
- Footer-option
- Landing-about-section
- Landing-benefit-section
- Landing-finder-section
- Landing-hero-section
- Landing-review-section
- Menu-hero-section
- Navbar
- Navbar-option
- Testimonial

#### 7. You are ready to go!

You can go to your local instance of Foodies and see the content of the CMS.

# 🚀 Strapi

Strapi comes with a full featured [Command Line Interface](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html) (CLI) which lets you scaffold and manage your project in seconds.

### `develop`

Start your Strapi application with autoReload enabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-develop)

```
npm run develop
# or
yarn develop
```

### `start`

Start your Strapi application with autoReload disabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-start)

```
npm run start
# or
yarn start
```

### `build`

Build your admin panel. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-build)

```
npm run build
# or
yarn build
```

## ⚙️ Deployment

Strapi gives you many possible deployment options for your project. Find the one that suits you on the [deployment section of the documentation](https://docs.strapi.io/developer-docs/latest/setup-deployment-guides/deployment.html).

## 📚 Learn more

- [Resource center](https://strapi.io/resource-center) - Strapi resource center.
- [Strapi documentation](https://docs.strapi.io) - Official Strapi documentation.
- [Strapi tutorials](https://strapi.io/tutorials) - List of tutorials made by the core team and the community.
- [Strapi blog](https://docs.strapi.io) - Official Strapi blog containing articles made by the Strapi team and the community.
- [Changelog](https://strapi.io/changelog) - Find out about the Strapi product updates, new features and general improvements.

Feel free to check out the [Strapi GitHub repository](https://github.com/strapi/strapi). Your feedback and contributions are welcome!

## ✨ Community

- [Discord](https://discord.strapi.io) - Come chat with the Strapi community including the core team.
- [Forum](https://forum.strapi.io/) - Place to discuss, ask questions and find answers, show your Strapi project and get feedback or just talk with other Community members.
- [Awesome Strapi](https://github.com/strapi/awesome-strapi) - A curated list of awesome things related to Strapi.

---

<sub>🤫 Psst! [Strapi is hiring](https://strapi.io/careers).</sub>
