module.exports = ({ env }) => ({
  'import-export-entries': {
    enabled: true,
    resolve: './src/plugins/import-export-entries',
  },
});
